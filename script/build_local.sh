#!/usr/bin/env bash

# Test if docker command exists
if ! [ -x "$(command -v docker)" ]; then
  podman build --compress ./$1
else
  docker build --compress ./$1
fi
