#!/usr/bin/env bash

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker buildx create --platform linux/amd64,linux/386,linux/arm64,linux/arm/v7,linux/arm/v6 --use --name builder
docker buildx ls
docker buildx build --compress \
  --platform linux/arm/v7,linux/arm64 --push \
  -t $CI_REGISTRY_IMAGE/$1:latest \
  -t $CI_REGISTRY_IMAGE/$1:$CI_COMMIT_SHORT_SHA \
  ./$1
