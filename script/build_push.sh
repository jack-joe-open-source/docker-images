#!/usr/bin/env bash

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

docker build --compress \
  -t $CI_REGISTRY_IMAGE/$1:latest \
  -t $CI_REGISTRY_IMAGE/$1:$CI_COMMIT_SHORT_SHA \
  ./$1

docker push -a $CI_REGISTRY_IMAGE/$1
